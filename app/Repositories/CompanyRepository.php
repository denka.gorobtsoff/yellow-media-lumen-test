<?php

namespace App\Repositories;

use App\Models\Company;
use App\Models\User;

class CompanyRepository
{
    public function create(array $data)
    {
        return Company::create($data);
    }

    public function findByUser(User $user) :array
    {
        return $user->companies()->get(['title', 'phone', 'description'])->toArray();
    }

    public function findById($id)
    {
        return Company::find($id);
    }
}
