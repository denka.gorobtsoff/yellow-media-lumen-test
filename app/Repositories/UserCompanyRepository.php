<?php

namespace App\Repositories;

use App\Models\UserCompany;

class UserCompanyRepository
{
    public function create(array $data)
    {
        return UserCompany::create($data);
    }
}
