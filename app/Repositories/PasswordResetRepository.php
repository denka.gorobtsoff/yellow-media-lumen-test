<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PasswordResetRepository
{
    public function createToken($email)
    {
        $token = bin2hex(random_bytes(30));

        DB::table('password_resets')->insert([
             'email' => $email,
             'token' => $token,
             'created_at' => Carbon::now(),
         ]);

        return $token;
    }

    public function getToken($token)
    {
        return DB::table('password_resets')->where('token', $token)->first();
    }

    public function deleteToken($email)
    {
        return DB::table('password_resets')->where('email', $email)->delete();
    }
}
