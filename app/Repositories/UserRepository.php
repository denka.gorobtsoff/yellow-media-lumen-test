<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function create(array $data)
    {
        return User::create($data);
    }

    public function findByEmail($email)
    {
        return User::where('email', $email)->first();
    }

    public function findByToken($token)
    {
        return User::where('recovery_token', $token)->first();
    }

    public function findById($id)
    {
        return User::find($id);
    }

}
