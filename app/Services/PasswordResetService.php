<?php

namespace App\Services;

use App\Repositories\PasswordResetRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class PasswordResetService
{
    protected $passwordResetRepository;
    protected $userRepository;

    public function __construct(PasswordResetRepository $passwordResetRepository, UserRepository $userRepository)
    {
        $this->passwordResetRepository = $passwordResetRepository;
        $this->userRepository = $userRepository;
    }

    public function sendResetEmail($email)
    {
        $token = $this->passwordResetRepository->createToken($email);

        // Отправка email
        Mail::raw("Use this token to reset your password: $token", function($message) use ($email) {
            $message->to($email)
                ->subject('Password Reset');
        });

        return $token;
    }

    public function resetPassword($token, $password)
    {
        $resetRecord = $this->passwordResetRepository->getToken($token);
        if (!$resetRecord) {
            return false;
        }

        $user = $this->userRepository->findByEmail($resetRecord->email);
        if (!$user) {
            return false;
        }

        $user->password = Hash::make($password);
        $user->save();

        $this->passwordResetRepository->deleteToken($resetRecord->email);

        return true;
    }
}

