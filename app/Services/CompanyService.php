<?php

namespace App\Services;

use App\Repositories\CompanyRepository;

class CompanyService
{
    protected $companyRepository;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    public function create(array $data)
    {
        return $this->companyRepository->create($data);
    }

    public function findByUser($userId)
    {
        return $this->companyRepository->findByUser($userId);
    }

    public function update($id, array $data, $userId)
    {
        $company = $this->companyRepository->findById($id);

        if ($company && $company->user_id == $userId) {
            $company->update($data);
            return $company;
        }

        return null;
    }

    public function delete($id, $userId)
    {
        $company = $this->companyRepository->findById($id);

        if ($company && $company->user_id == $userId) {
            return $company->delete();
        }

        return false;
    }
}


