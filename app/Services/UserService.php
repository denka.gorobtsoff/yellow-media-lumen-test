<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function register(array $data)
    {
        $data['password'] = Hash::make($data['password']);
        return $this->userRepository->create($data);
    }

    public function findByEmail($email)
    {
        return $this->userRepository->findByEmail($email);
    }

    public function findByToken($token)
    {
        return $this->userRepository->findByToken($token);
    }

    public function update($id, array $data)
    {
        $user = $this->userRepository.findById($id);

        if ($user) {
            $user->update($data);
            return $user;
        }

        return null;
    }
}


