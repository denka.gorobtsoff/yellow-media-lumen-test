<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:6',
            'phone' => 'required|string',
        ]);

        $user = $this->userService->register($request->all());

        if (!$user) {
            return response()->json(['message' => 'Some problem with registration!'], 404);
        }

        return response()->json(['message' => 'Success registration!'], 201);
    }

    public function signIn(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = $request->only('email', 'password');

        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return response()->json(compact('token'));
    }

    public function recoverPassword(Request $request)
    {
        $this->validate($request, ['email' => 'required|string|email']);

        $user = $this->userService->findByEmail($request->email);

        if (!$user) {
            return response()->json(['error' => 'User not found'], 404);
        }

        // Generate token and send recovery email
        $token = Str::random(60);
        $user->recovery_token = $token;
        $user->save();

        // Send email with the recovery token
        Mail::send('emails.recover', ['token' => $token], function ($m) use ($user) {
            $m->to($user->email)->subject('Password Recovery');
        });

        return response()->json(['message' => 'Recovery email sent']);
    }

    public function resetPassword(Request $request)
    {
        $this->validate($request, [
            'token' => 'required|string',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = $this->userService->findByToken($request->token);

        if (!$user) {
            return response()->json(['error' => 'Invalid token'], 400);
        }

        $user->password = app('hash')->make($request->password);
        $user->recovery_token = null;
        $user->save();

        return response()->json(['message' => 'Password reset successful']);
    }


}
