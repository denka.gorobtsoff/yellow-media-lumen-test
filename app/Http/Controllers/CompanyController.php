<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CompanyService;
use Tymon\JWTAuth\Facades\JWTAuth;

class CompanyController extends Controller
{
    protected $companyService;

    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    public function index()
    {
        $user = JWTAuth::parseToken()->authenticate();

        if (!$user) {
            return response()->json(['error' => 'Unauthorized or not found'], 404);
        }

        $companies = $this->companyService->findByUser($user);
        return response()->json($companies);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'phone' => 'required|string',
            'description' => 'required|string',
        ]);

        $user = JWTAuth::parseToken()->authenticate();
        $data = $request->all();
        $data['user_id'] = $user->id;

        $company = $this->companyService->create($data);
        return response()->json($company, 201);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'sometimes|required|string',
            'phone' => 'sometimes|required|string',
            'description' => 'sometimes|required|string',
        ]);

        $user = JWTAuth::parseToken()->authenticate();
        $company = $this->companyService->update($id, $request->all(), $user->id);

        if (!$company) {
            return response()->json(['error' => 'Unauthorized or not found'], 404);
        }

        return response()->json(['success' => 'Company '. $company->title . ' updated!'], 404);
    }

    public function destroy($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $deleted = $this->companyService->delete($id, $user->id);

        if (!$deleted) {
            return response()->json(['error' => 'Unauthorized or not found'], 404);
        }

        return response()->json(['deleted' => 'Company deleted!'], 204);
    }
}


