<?php

namespace App\Http\Controllers;

use App\Services\PasswordResetService;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class PasswordResetController extends Controller
{
    protected $passwordResetService;

    public function __construct(PasswordResetService $passwordResetService)
    {
        $this->passwordResetService = $passwordResetService;
    }

    public function sendResetEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);
        $email = $request->input('email');
        $token = $this->passwordResetService->sendResetEmail($email);
        return response()->json(['message' => 'Password reset email sent.', 'token' => $token], 200);
    }

    public function resetPassword(Request $request)
    {
        $this->validate($request, [
            'token' => 'required|string',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $token = $request->input('token');
        $password = $request->input('password');
        $success = $this->passwordResetService->resetPassword($token, $password);

        if (!$success) {
            return response()->json(['message' => 'Invalid token or email.'], 400);
        }

        return response()->json(['message' => 'Password has been reset.'], 200);
    }
}

