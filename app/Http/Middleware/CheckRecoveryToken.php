<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class CheckRecoveryToken
{
    public function handle($request, Closure $next)
    {
        $token = $request->query('token');

        if (!$token || !User::where('recovery_token', $token)->exists()) {
            return response()->json(['error' => 'Invalid or expired token'], 400);
        }

        return $next($request);
    }
}
