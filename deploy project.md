## Hello in test project for Lumen

You already have Lumen framework and you need only make some console commands for checking this functionality.

## check your .env file please (you can get settings from .env.examle)

### First you need install composer 

composer install

### and 

php artisan jwt:secret

### Second step 

php artisan migrate

php artisan db:seed

### And start your server with command 

php -S localhost:8000 -t public
