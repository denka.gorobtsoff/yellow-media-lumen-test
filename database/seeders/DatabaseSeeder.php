<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Faker\Factory as Faker;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        User::factory()->count(10)->create();

        foreach (range(1, 10) as $index) {
            DB::table('companies')->insert([
               'title' => $faker->word(2),
               'phone' => $faker->phoneNumber(),
               'description' => $faker->text(),
               'user_id' => User::all()->random()->id,
               'created_at' => Carbon::now(),
               'updated_at' => Carbon::now()
           ]);
        }
    }
}
